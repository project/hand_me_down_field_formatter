<?php
/**
 * @file
 * The Hand me down field formatter module.
 */

/**
 * Implements hook_field_widget_info().
 */
function hand_me_down_field_formatter_field_formatter_info() {
  return array(
    'hand_me_down_field_formatter_entity_view' => array(
      'label' => t('Rendered entity (hand-me-down fields)'),
      'description' => t('Display the referenced entities rendered by entity_view() and hand-me-down fields.'),
      'field types' => array('entityreference'),
      'settings' => array(
        'view_mode' => '',
        'hand_me_down_fields' => array(),
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function hand_me_down_field_formatter_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = &$instance['display'][$view_mode];
  $settings = &$display['settings'];

  // Get the default entity reference form.
  $display['type'] = 'entityreference_entity_view';
  $element = entityreference_field_formatter_settings_form($field, $instance, $view_mode, $form, $form_state);
  $display['type'] = 'hand_me_down_field_formatter_entity_view';

  // Now add the extra settings for the fields to be copied over.
  $element['hand_me_down_fields'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Hand-me-down fields'),
    '#default_value' => $settings['hand_me_down_fields'],
  );
  // Find the fields that could be handed down.
  $my_fields = field_info_instances($instance['entity_type'], $instance['bundle']);
  $their_bundled_fields = field_info_instances($field['settings']['target_type']);

  foreach ($their_bundled_fields as $bundle => $their_fields) {
    $similar_fields = array_intersect_key($my_fields, $their_fields);
    foreach ($similar_fields as $similar_field_name => $similar_field) {
      $element['hand_me_down_fields']['#options'][$similar_field_name] = $similar_field['label'];
    }
  }
  if (!empty($element['hand_me_down_fields']['#options'])) {
    asort($element['hand_me_down_fields']['#options']);
  }

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function hand_me_down_field_formatter_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = &$instance['display'][$view_mode];
  $settings = &$display['settings'];

  $display['type'] = 'entityreference_entity_view';
  $summary = entityreference_field_formatter_settings_summary($field, $instance, $view_mode);
  $display['type'] = 'hand_me_down_field_formatter_entity_view';

  $summary = explode('<br />', $summary);

  if (!isset($settings['hand_me_down_fields'])) {
    $settings['hand_me_down_fields'] = array();
  }
  else {
    $settings['hand_me_down_fields'] = array_filter($settings['hand_me_down_fields']);
  }

  $summary[] = format_plural(count($settings['hand_me_down_fields']), '1 field handed down', '@count fields handed down');

  return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_prepare_view().
 */
function hand_me_down_field_formatter_field_formatter_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items, $displays) {
  // Change the displays being used before we call through to entityreference.
  $changed_displays = array();
  foreach ($displays as $key => $display) {
    if ($display['type'] == 'hand_me_down_field_formatter_entity_view')  {
      $displays[$key]['type'] = 'entityreference_entity_view';
      $changed_displays[] = $key;
    }
  }
  entityreference_field_formatter_prepare_view($entity_type, $entities, $field, $instances, $langcode, $items, $displays);
  // Now change the types back.
  foreach ($changed_displays as $key) {
    $displays[$key]['type'] = 'hand_me_down_field_formatter_entity_view';
  }
}

/**
 * Implements hook_field_formatter_view().
 *
 * This looks a lot like the formatter from
 * entityreference_field_formatter_view().
 */
function hand_me_down_field_formatter_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $result = array();

  if (!isset($display['settings']['hand_me_down_fields'])) {
    $display['settings']['hand_me_down_fields'] = array();
  }
  else {
    $display['settings']['hand_me_down_fields'] = array_filter($display['settings']['hand_me_down_fields']);
  }

  foreach ($items as $delta => $item) {
    // Protect ourselves from recursive rendering.
    static $depth = 0;
    $depth++;
    if ($depth > 20) {
      throw new EntityReferenceRecursiveRenderingException(t('Recursive rendering detected when rendering entity @entity_type(@entity_id). Aborting rendering.', array('@entity_type' => $entity_type, '@entity_id' => $item['target_id'])));
    }

    $referenced_entity = clone $item['entity'];

    // Hand the fields down.
    if (!empty($display['settings']['hand_me_down_fields'])) {
      // Let's find out fields we could possibly copy.
      $my_fields = field_info_instances($instance['entity_type'], $instance['bundle']);
      list($id, $vid, $bundle) = entity_extract_ids($field['settings']['target_type'], $referenced_entity);
      $their_fields = field_info_instances($field['settings']['target_type'], $bundle);

      $shared_fields = array_intersect_key($my_fields, $their_fields, $display['settings']['hand_me_down_fields']);
      foreach ($shared_fields as $shared_field_name => $shared_field) {
        $referenced_entity->{$shared_field_name} = $entity->$shared_field_name;
      }
    }

    unset($referenced_entity->content);
    $result[$delta] = entity_view($field['settings']['target_type'], array($item['target_id'] => $referenced_entity), $display['settings']['view_mode'], $langcode, FALSE);
    $depth = 0;
  }

  return $result;
}
